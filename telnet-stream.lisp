;;;; telnet-stream.lisp -- Telnet I/O stream definition.
;;;;
;;;; $Id$
;;;;
;;;; Copyfnord (K) 3169 - 3170 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; This file implements a Telnet protocol stream as a wrapper around a
;;;; FILE-STREAM which is created for the Telnet connection socket. Not all
;;;; stream functions are implemented for TELNET-STREAMs, and there are some
;;;; additional ones specific to the Telnet protocol.
;;;;
;;;; Also within this file are the definitions for Telnet protocol commands
;;;; and options and the functions which read them from TELNET-STREAMs and
;;;; process them accordingly.

(in-package #:ltelnetd)


  ;;
;;;;;; Telnet Commands and Options.
  ;;

;;; Telnet commands and options are sent in band but the client and server
;;; must filter them out of the stream. All command and option sequences are
;;; preceded by the IAC ("Interpret As Command") character.
;;;
;;; Telnet options are negotiated between client and server. They are argued
;;; by means of the DO, DONT, WILL, and WONT commands. FIXME ...
;;;
;;; Some options are simple, just saying "IAC DO FOO" is sufficient to turn on
;;; FOO. Some are much more complicated. These complex options have specific
;;; subnegotiation patterns, all of which are started with "IAC SB FOO" and
;;; ended with "IAC SE FOO".

;;; IAC
(defconstant +iac+                  (code-char 255) "IAC")

;;; IAC Commands.
;;;
;;; These are commands that may be issued or received (with preceding IAC) at
;;; any point in the Telnet stream.
(defconstant +cmd-bell+             (code-char 7)   "Bell")
(defconstant +cmd-se+               (code-char 240) "Subnegotiation End")
(defconstant +cmd-nop+              (code-char 241) "No Operation")
(defconstant +cmd-dm+               (code-char 242) "Data Mark")
(defconstant +cmd-brk+              (code-char 243) "Break")
(defconstant +cmd-ip+               (code-char 244) "Interrupt Process")
(defconstant +cmd-ao+               (code-char 245) "Abort Output")
(defconstant +cmd-ayt+              (code-char 246) "Are You There?")
(defconstant +cmd-ec+               (code-char 247) "Erase Character")
(defconstant +cmd-el+               (code-char 248) "Erase Line")
(defconstant +cmd-ga+               (code-char 249) "Go Ahead")
(defconstant +cmd-sb+               (code-char 250) "Subnegotiation Begin")
(defconstant +cmd-will+             (code-char 251) "Will")
(defconstant +cmd-wont+             (code-char 252) "Won't")
(defconstant +cmd-do+               (code-char 253) "Do")
(defconstant +cmd-dont+             (code-char 254) "Don't")

;;; Option Codes.
;;;
;;; These are options which may only be received as part of an option
;;; negotiation (ie, preceded by "IAC {DO,DONT,WILL,WONT,SB,SE}").
(defconstant +opt-echo+             (code-char 1)   "Echo")
(defconstant +opt-supress-go-ahead+ (code-char 3)   "Suppress Go Ahead")
(defconstant +opt-status+           (code-char 5)   "Status")
(defconstant +opt-timing-mark+      (code-char 6)   "Timing Mark")
(defconstant +opt-terminal-type+    (code-char 24)  "Terminal Type")
(defconstant +opt-end-of-record+    (code-char 25)  "End of Record")
(defconstant +opt-ttyloc+           (code-char 26)  "Terminal Location Number")
(defconstant +opt-naws+             (code-char 31)  "Negotiate About Window Size")
(defconstant +opt-terminal-speed+   (code-char 32)  "Terminal Speed")
(defconstant +opt-toggle-flow-ctrl+ (code-char 33)  "Toggle Flow Control")
(defconstant +opt-linemode+         (code-char 34)  "Line Mode")
(defconstant +opt-xdisploc+         (code-char 35)  "X Display Location")
(defconstant +opt-environ+          (code-char 36)  "Environment")
(defconstant +opt-eor+              (code-char 239) "End of Record")
(defconstant +opt-exopl+            (code-char 255) "Extended Options List")
(defconstant +opt-randomly-lose+    (code-char 256) "Randomly Lose")
(defconstant +opt-submsg+           (code-char 257) "Subliminal Message")

;;; Not really an option, this is a special char used for option
;;; subnegotiation among other things.
(defconstant +opt-is+               (code-char 0)   "IS")


  ;;
;;;;;; Telnet Stream
  ;;

;;; TELNET-STREAM isn't a real stream. It merely pretends to be one by
;;; implementing all the applicable stream functions, and functions as a
;;; wrapper around whatever stream has been created from the socket.
(defclass telnet-stream ()
  ((stream :accessor telnet-stream-stream :type (or file-stream null))
   (transmit-mode :initform :character :type (member :character :line)
                  :accessor telnet-stream-transmit-mode)
   (remote-echo :initform t :type (member nil t)
                :accessor telnet-stream-remote-echo)
   (local-suppress-go-ahead :initform nil :type (member nil t)
                            :accessor telnet-stream-local-suppress-go-ahead)
   (remote-suppress-go-ahead :initform nil :type (member nil t)
                             :accessor telnet-stream-remote-suppress-go-ahead)
   (resume-buffer :initform nil :accessor telnet-stream-resume-buffer)
   (resume-urgent-length :initform nil :accessor telnet-stream-urgent-length)
   (bell-handler-function :initform nil :type (or function null)
                          :accessor telnet-stream-bell-handler-function)
   (naws-handler-function :initform nil :type (or function null)
                          :accessor telnet-stream-naws-handler-function)))

  ;;
;;;;;; Read methods.
  ;;

(defmethod read-char ((stream telnet-stream) &optional eof-error-p eof-value recursive-p)
  "Read a character from the telnet-stream STREAM.

Telnet commands (ie, sequences begun with the IAC character) are automatically
handled using the PROCESS-COMMAND method and any applicable handler functions
stored in slots within the stream object."
  (declare (ignore (eof-error-p eof-value recursive-p))) ;FIXME: Don't ignore.
  (with-slots ((true-stream stream)) stream
    ;; Read a character and if it's IAC then call PROCESS-COMMAND on the
    ;; stream. Keep doing so until a non-IAC character is found, which return.
    (loop for char = (cl:read-char true-stream)
          until (not (char-equal char +iac+))
          do (progn
               (cl:unread-char char true-stream) ;put back IAC character
               (process-command stream))
          finally (return-from read-char char)))))

(defmethod read-char-no-hang ((stream telnet-stream) &optional input-stream eof-error-p eof-value recursive-p)
  (with-slots ((true-stream stream)) stream
    ;; FIXME: Verify if this works with peeked chars.
    (if (cl:listen true-stream)
        (read-char stream))))

(defmethod discard-char ((stream telnet-stream))
  "Silently discard (read and throw away) a character from STREAM. Always
returns T."
  (cl:read-char (telnet-stream-stream stream))
  (values t))

(defmethod unread-char (char (stream telnet-stream))
  "Unread a character CHAR from the telnet-stream STREAM."
  (cl:unread-char char (telnet-stream-stream stream)))

(defmethod peek-char (peek-type (stream telnet-stream) &optional eof-error-p eof-value recursive-p)
  "Do the same thing that CL:PEEK-CHAR does."
  (cl:peek-char peek-type (telnet-stream-stream stream) eof-error-p eof-value recursive-p))

(defmethod read-command ((stream telnet-stream))
  "Reads a command sequence from STREAM and returns the command sequence. If
no IAC character is found at start of STREAM then NIL is returned.

A command sequence may be a simple IAC FOO command, a subnegotiation sequence
begun with IAC SB FOO and ending with IAC SE, or an option with IAC {DO,DONT,
WILL,WONT} FOO."
  (with-slots ((true-stream stream)) stream
    (if (not (char-equal (cl:peek-char nil true-stream) +iac+))
        ;; If we don't immediately see IAC then lose.
        (return-from read-command nil)
        ;; Otherwise try reading the command sequence.
        (flet ((read-subnegotiation (cmd stm)
                 ;; Read until we get IAC SE.
                 ;; FIXME: Better way to check for both IAC and SE?
                 (loop for char = (cl:read-char stm)
                       until (and (char-equal char +cmd-se+)
                                  (char-equal (elt cmd (1- (length cmd))) +iac+))
                       do (setf cmd (concatenate 'string cmd char))
                       finally (setf cmd (concatenate 'string cmd char))) ;keep SE
                 (return-from read-command cmd))
               (read-option (cmd stm)
                 ;; Read option character. There's only one.
                 (setf cmd (concatenate 'string cmd (read-char stm)))
                 (return-from read-command cmd)))
          (let ((cmdstr (concatenate 'string (cl:read-char true-stream)
                                             (cl:read-char true-stream))))
            (case (char 2 cmdstr)
              (+cmd-sb+ (read-subnegotiation cmdstr stream))
              ((+cmd-will+ +cmd-wont+ +cmd-do+ +cmd-dont+)
               (read-option cmdstr stream))
              ;; If it's an ordinary command then just return it.
              (t cmdstr)))))))

  ;;
;;;;;; Write methods.
  ;;

;;; The write methods in most cases simply pass control on to the CL function
;;; of the same name, calling that function on the real stream. These should
;;; be used instead of the normal stream functions because they know how to
;;; find the real stream, whereas the generic stream functions don't.

(defmethod write-char (char (stream telnet-stream))
  (with-slots ((true-stream stream)) stream
    (cl:write-char char true-stream))))

(defmethod write-string (string (stream telnet-stream) &key start end &allow-other-keys)
  (with-slots ((true-stream stream)) stream
    (cl:write-string string true-stream :start start :end end)))

(defmethod write-line (string (stream telnet-stream) &key start end &allow-other-keys)
  (with-slots ((true-stream stream)) stream
    (cl:write-line string true-stream :start start :end end)))

(defmethod write-command ((stream telnet-stream) (command character))
  (with-slots ((true-stream stream)) stream
    (cl:write-char +iac+ true-stream)
    (cl:write-char command true-stream)))

(defmethod terpri ((stream telnet-stream))
  (with-slots ((true-stream stream)) stream
    (cl:terpri true-stream)))

(defmethod fresh-line ((stream telnet-stream))
  (with-slots ((true-stream stream)) stream
    (cl:fresh-line true-stream)))


  ;;
;;;;;; Command and Option Handling.
  ;;

(defmethod process-command ((stream telnet-stream))
  (let ((cmdstr (read-command stream)))
    (case (char 2 cmdstr)
      (+cmd-sb+ (subnegotiate-option stream cmdstr))
      ((+cmd-will+ +cmd-wont+ +cmd-do+ +cmd-dont+)
       (negotiate-option stream cmdstr))
      (+cmd-bell+
       (funcall (telnet-stream-bell-handler-function stream) stream)))))

;;; Subnegotiable options and their subnegotiation handlers are stored in
;;; *OPTION-TABLE*. The reason is that the Telnet protocol continually has new
;;; options added to it, and implementors may wish to experiment with their
;;; own additional options. Thus every option negotiation request is looked up
;;; in the table for an appropriate handler. New option handlers can easily be
;;; defined and added at run time, and the incidence of negotiation requests
;;; is small so the additional cost of lookup is minimal.

(defvar *option-table*
  (make-hash-table))

(defun add-subnegotiable-option (key handler)
  (setf (gethash key *option-table*) handler))

(defmethod subnegotiate-option ((stream telnet-stream) (cmdstr string))
  (let ((option (char 3 cmdstr))
        (handler (gethash option *option-table*)))
    (if handler
        (funcall handler stream cmdstr)
        (values))))

;;; NAWS
;;;
;;; The subnegotiation sequence has the format:
;;;
;;;   IAC SB WIDTH-HI WIDTH-LO HEIGHT-HI HEIGHT-LO IAC SE
;;;
;;; where WIDTH-HI and WIDTH-LO are the high and low bytes of a 16 bit value
;;; indicating the terminal's screen width. Similarly for HEIGHT-HI and
;;; HEIGHT-LO.
(defmethod subnegotiate-naws-option ((stream telnet-stream) (cmdstr string))
  (let* ((width (logior (ash (char-code (char 4 cmdstr)) 8)
                        (char-code (char 5 cmdstr))))
         (height (logior (ash (char-code (char 6 cmdstr)) 8)
                         (char-code (char 7 cmdstr)))))
    (funcall (telnet-stream-naws-handler-function stream) width height)
    (values)))

(add-subnegotiable-option +opt-naws+ #'subnegotiate-naws-option)
