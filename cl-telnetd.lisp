;;;; ltelnetd.lisp -- Lisp Telnet daemon.
;;;;
;;;; $Id: ltelnetd.lisp,v 1.6 2004/01/30 06:20:29 james Exp $
;;;;
;;;; Copyfnord (K) 3170 James A. Crippen <james@unlambda.com>,
;;;;                    Martin Rydstrom <rydis@cd.chalmers.se>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;; NOTA BENE: This code doesn't work yet. It hasn't even been compiled. ;;;;
;;;; Don't expect it to do what you expect, or even what it expects.      ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; This is a Lisp Telnet daemon. To make it run, instantiate a TELNET-
;;;; LISTENER class with your favorite address and port (which default to
;;;; 0.0.0.0, ie the local network address, and port 23), then call START-
;;;; LISTENING on your new listener. To make it do something useful, stick a
;;;; function that accepts an IO stream into the HANDLER slot of the listener.
;;;; To stop listening call STOP-LISTENING (surprise surprise) on the
;;;; listener, which closes all connected sessions.
;;;;
;;;; Much of the socket code has been stolen rather obviously from Araneida.
;;;; All the nonthreaded, non-SBCL stuff from Araneida has been taken out,
;;;; however. The TELNET-LISTENER class is a container that holds the various
;;;; info on the listener and the listener threads. The MASTER-THREAD slot
;;;; contains the thread which watches the listener threads and runs new ones
;;;; when they have quit. Threads are represented by the TELNET-THREAD
;;;; structure which is simply a handle containing the pid and some metadata
;;;; about the thread.
;;;;
;;;; TODO:
;;;;  - Finish Telnet stream implementation
;;;;  - Q Method option negotiation (see RFC 1143)
;;;;  - Telnet NVT stuff
;;;;  - NAWS and TERMINAL-TYPE
;;;;  - All those other nifty options
;;;;  - Useful documentation (hah!)
;;;;  - CL-TERMINFO linkage (eg, so server Terminfo can override Telnet NVT)
;;;;  - Telnet client?
;;;;  - Environment option
;;;;  - SUPDUP support
;;;;
;;;; REFERENCES:
;;;;  RFC 652 "Telnet Output Carriage-Return Disposition Option"
;;;;  RFC 653 "Telnet Output Horizontal Tabstops Option"
;;;;  RFC 654 "Telnet Output Horizontal Tab Disposition Option"
;;;;  RFC 655 "Telnet Output Formfeed Disposition Option"
;;;;  RFC 656 "Telnet Output Vertical Tabstops Option"
;;;;  RFC 657 "Telnet Output Vertical Tab Disposition Option"
;;;;  RFC 658 "Telnet Output Linefeed Disposition"
;;;;  RFC 698 "Telnet Extended ASCII Option"
;;;;  RFC 726 "Remote Controlled Transmission and Echoing Telnet Option"
;;;;  RFC 727 "Telnet Logout Option"
;;;;  RFC 732 "Telnet Data Entry Terminal Option"
;;;;  RFC 735 "Revised Telnet Byte Macro Option"
;;;;  RFC 736 "Telnet SUPDUP Option"
;;;;  RFC 748 "Telnet Randomly-Lose Option"
;;;;  RFC 749 "Telnet SUPDUP-Output Option"
;;;;  RFC 779 "Telnet Send-Location Option"
;;;;  RFC 854 "Telnet Protocol Specification"
;;;;  RFC 855 "Telnet Option Specifications"
;;;;  RFC 856 "Telnet Binary Translation"
;;;;  RFC 857 "Telnet Echo Option"
;;;;  RFC 858 "Telnet Suppress Go Ahead Option"
;;;;  RFC 859 "Telnet Status Option"
;;;;  RFC 860 "Telnet Timing Mark Option"
;;;;  RFC 861 "Telnet Extended Options - List Option"
;;;;  RFC 885 "Telnet End of Record Option"
;;;;  RFC 930 "Output Marking Telnet Option"
;;;;  RFC 946 "Telnet Terminal Location Number Option"
;;;;  RFC 947 "TACACS User Identification Telnet Option"
;;;;  RFC 1043 "Telnet Data Entry Terminal Option: DODIIS Implementation"
;;;;  RFC 1053 "Telnet X.3 PAD Option"
;;;;  RFC 1073 "Telnet Window Size Option" (NAWS)
;;;;  RFC 1079 "Telnet Terminal Speed Option"
;;;;  RFC 1091 "Telnet Terminal-Type Option"
;;;;  RFC 1096 "Telnet X Display Location Option"
;;;;  RFC 1097 "Telnet Subliminal Message Option"
;;;;  RFC 1143 "The Q Method of Implementing Telnet Option Negotiation"
;;;;  RFC 1184 "Telnet Linemode Option"
;;;;  RFC 1372 "Telnet Remote Flow Control Option"
;;;;  RFC 1408 "Telnet Environment Option"
;;;;  RFC 1412 "Telnet Authentication: SPX"
;;;;  RFC 1571 "Telnet Environment Option Interoperability Issues"
;;;;  RFC 1572 "Telnet Environment Option"
;;;;  RFC 2066 "Telnet Charset Option"
;;;;  RFC 2217 "Telnet Com Port Control Option"
;;;;  RFC 2840 "Telnet Kermit Option"
;;;;  RFC 2941 "Telnet Authentication Option"
;;;;  RFC 2942 "Telnet Authentication: Kerberos Version 5"
;;;;  RFC 2943 "Telnet Authentication Using DSA"
;;;;  RFC 2944 "Telnet Authentication: SRP"
;;;;  RFC 2946 "Telnet Data Encryption Option"
;;;;  RFC 2947 "Telnet Encryption: DES3 64 bit Cipher Feedback"
;;;;  RFC 2948 "Telnet Encryption: DES3 64 bit Output Feedback"
;;;;  RFC 2949 "Telnet Encryption: CAST-128 64 bit Output Feedback"
;;;;  RFC 2950 "Telnet Encryption: CAST-128 64 bit Cipher Feedback"
;;;;  RFC 2951 "Telnet Authentication Using KEA and SKIPJACK"
;;;;  RFC 2952 "Telnet Encryption: DES 64 bit Cipher Feedback"
;;;;  RFC 2953 "Telnet Encryption: DES 64 bit Output Feedback"

#-sbcl (progn
         (format t "LTELNETD does not run except with SBCL. Try porting it.")
         (quit))

(require #:sb-bsd-sockets)

(defpackage #:ltelnetd
  (:use #:common-lisp #:sb-bsd-sockets)
  (:export #:run-telnet-listener))

(in-package #:ltelnetd)


  ;;
;;;;;; Classes.
  ;;

;;; FIXME: Add :type to slots for optimizability.
(defclass telnet-listener ()
  ((handler :initform #'generic-telnet-handler :initarg :handler
            :accessor telnet-listener-handler
            :documentation "Function to run when connection is accepted.")
   (overflow-handler :initform #'generic-telnet-overflow-handler
                     :initarg :overflow-handler
                     :accessor telnet-listener-overflow-handler
                     :documentation "Function to handle connections during overflow.")
   (address :initform #(0 0 0 0) :initarg :address
            :accessor telnet-listener-address
            :documentation "Address on which to listen.")
   (port :initform 23 :initarg :port :accessor telnet-listener-port
         :documentation "Port on which to listen.")
   (socket :initform nil :accessor telnet-listner-socket
           :documentation "Socket of listener.")
   (master-thread :initform nil :accessor telnet-listener-master-thread
                  :documentation "Master dispatching thread.")
   (master-thread-sleep-time :initform 60
                             :accessor telnet-listener-master-thread-sleep-time
                             :documentation "Master thread sleep time.")
   (max-spare :initform 5 :initarg :max-spare
              :accessor telnet-listener-max-spare
              :documentation "Maximum number of spare listening threads.")
   (min-spare :initform 1 :initarg :min-spare
              :accessor telnet-listener-min-spare
              :documentation "Minimum number of spare listening threads.")
   (threads :initform nil :accessor telnet-listener-threads
            :documentation "Listening threads.")))

(defstruct telnet-thread
  pid
  last-hit
  (quitting nil)
  (state created :type (member created accepting connected finished dead nil)))


  ;;
;;;;;; Utility Functions.
  ;;

;;; Looks bugless, but should be kept synched with Dan's original version in
;;; Araneida. Or maybe obviated someday, though that's unlikely.
(defun forcibly-close-stream (s)
  "Blow away the stream S no matter what it takes. Tries to do so nicely, then
becomes successively crueler, finally using SB-UNIX:UNIX-CLOSE on the offending
stream's file descriptor."
  (let ((fd (sb-sys:fd-stream-fd s)))
    (multiple-value-bind (r e) (ignore-errors (close s) t)
      (unless r
	(format t "Unable to close fd ~A: ~A, trying harder ~%" fd e)
	(multiple-value-bind (r e) (ignore-errors (close s :abort t) t)
	  (unless r
	    (format t "still unable to close ~A: ~A, try harder ~%" fd e)
	    (multiple-value-bind (r e)
		(ignore-errors (sb-unix:unix-close fd) t)
	      (unless r
		(format t "Even unix-close failed on ~A:~A, giving up~%"
			fd e)))))))))


  ;;
;;;;;; Listeners.
  ;;

(defun generic-telnet-handler (stream listener)
  "This generic Telnet handler used if no other is supplied. Merely tells the
user that the daemon is running and accepting connections. Returns nothing."
  (format stream "~&Welcome to LTELNETD, the Lisp Telnet Daemon.")
  (format stream "~&You have connected to ~A on port ~A."
          (telnet-listener-address listener)
          (telnet-listener-port listener))
  (format stream "~&There's nothing running here right now.")
  (format stream "~&This has been the generic telnet listener.~%Bye.~%")
  (values))

(defun generic-telnet-overflow-handler (stream listener)
  "This generic overflow handler is used if no other is supplied. Tells the
user that no further connections are being accepted and closes the
connection. Returns nothing."
  (format stream "~&You have connected to ~A on port ~A."
          (telnet-listener-address listener)
          (telnet-listener-port listener))
  (format stream "~&No further connections are being accepted. Please try again later.")
  (values))

(defmethod accept-connection ((listener telnet-listener) (thread telnet-thread))
  "Puts the Telnet listener thread THREAD into a listening state, awaiting new
connections."
  (setf (telnet-thread-state thread) 'accepting)
  (flet ((accept (listener)
           "Accepts a connection on LISTENER's socket, returns a stream of the connected socket."
           (multiple-value-bind (socket peer port)
               (socket-accept (telnet-listener-socket listener))
             (socket-make-stream socket :element-type 'base-char
                                 :input t :output t :buffering :none
                                 :name (format nil "Telnet connection from ~A, port ~A"
                                               peer port)))))
    ;; The ACCEPT below will block until a connection is made.
    (let ((stream (accept listener)))
      (setf (telnet-thread-state thread) 'connected)
      (unwind-protect
           (funcall (telnet-listener-handler listener) stream listener)
        (setf (telnet-thread-state thread) 'finished)
        (setf (telnet-thread-last-hit thread) (get-universal-time))
        (forcibly-close-stream stream)))))

;;; FIXME: Question - Is there a reason that this isn't a method of TELNET-
;;; LISTENER?
(defun make-listener-thread (listener)
  "Creates a Telnet listener thread."
  (declare (type telnet-listener listener))
  (let ((thread (make-telnet-thread)))
    (setf (telnet-thread-pid thread)
          (sb-thread:make-thread
           (lambda ()
             (loop (accept-connection listener thread)))))
    (values thread)))

(defmethod cycle-master-thread ((listener telnet-listener))
  "Execute one cycle of the master thread, doing bookkeeping on listener
threads, spawning new threads and killing stale ones."
  (let ((min (telnet-listener-min-spare listener))
        (max (telnet-listener-max-spare listener))
        (spares 0))
    ;; Loop over all children and add new ones if necessary, or kill ones that
    ;; are stale. If there's nothing to do then sleep for a while.
    (dolist (this-thread (telnet-listener-threads listener))
      (when (numberp (telnet-thread-last-hit this-thread))
        (cond ((not (zerop (sb-unix:unix-kill (telnet-thread-pid this-thread) 0)))
               (format t "~&;; This thread (pid ~D) is dead~%"
                       (telnet-thread-pid this-thread))
               (setf (telnet-thread-pid this-thread) 0))
              ((> max spares)
               (format t "~&;; Thread ~A last used ~A, ~A spare~%"
                       (telnet-thread-pid this-thread)
                       (telnet-thread-last-hit this-thread)
                       spares)
               (incf spares))
              (t (format t "~&;; Thread ~A last used ~A, killing it~%"
                         (telnet-thread-pid this-thread)
                         (telnet-thread-last-used this-thread))
                 (sb-thread:terminate-thread (telnet-thread-pid this-thread))
                 (setf (telnet-thread-pid this-thread) 0)))))
    ;; Zero is a valid pid value but impossible for us to have.
    (setf (telnet-listener-threads listener)
          (remove-if #'zerop (telnet-listener-threads listener)
                     :key #'telnet-thread-pid))
    (when (< spares min)
      (dotimes (i (- min spares))
        (format t "~&;; Count ~A spare threads < ~A, adding one more~%"
                spares min)
        (push (make-listener-thread (listener))
              (telnet-listener-threads listener))))
    (when (<= min spares max)
      (sleep 60))
    ;; FIXME: Do something about spares > max.
    (sleep 1)))

(defmethod start-listening ((listener telnet-listener) &key (num-threads 5))
  "Starts the Telnet daemon with the number of listener threads specified by
NUM-THREADS, defaulting to five. Returns the value of the MASTER-THREAD slot
in LISTENER."
  (let ((socket (make-instance 'inet-socket :type :stream :protocol :tcp)))
    (setf (sockopt-reuse-address socket) t)
    (setf (sockopt-receive-buffer socket) 0)
    (socket-bind socket (telnet-listener-address listener)
                        (telnet-listener-port listener))
    (socket-listen socket 5)
    (setf (telnet-listener-socket listener) socket)
    (dotimes (i num-threads)
      (push (make-listener-thread listener) (telnet-listener-threads listener)))
    (setf (telnet-listener-master-thread listener)
          (sb-thread:make-thread
           (lambda ()
             (loop (cycle-master-thread listener)))))))

(defmethod stop-listening ((listener telnet-listener) &key abort
                           &allow-other-keys)
  "Stops the Telnet daemon and closes all open connections."
  (declare (ignore abort))              ;FIXME: always aborting, fix this
  (dolist (thread (telnet-listener-threads listener))
    (when (zerop (sb-unix:unix-kill (telnet-thread-pid thread) 0))
      (sb-thread:terminate-thread (telnet-thread-pid thread))))
  (when (telnet-listener-master-thread listener)
    (sb-thread:terminate-thread (telnet-listener-master-thread listener)))
  (socket-close (telnet-listener-socket listener))
  (setf (telnet-listener-threads listener) nil
        (telnet-listener-master-thread listener) nil))
